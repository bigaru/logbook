#!/bin/sh

js="elm.js"
min="elm.min.js"
gzip="elm.min.js.gz"

# clean
rm -rf public
mkdir public

# build
cp -f assets/* public/
elm make src/Main.elm --optimize --output=public/$js

# optimize
uglifyjs public/$js --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' | uglifyjs --mangle --output=public/$min
gzip -k public/$min

echo "Compiled size:$(cat public/$js | wc -c) bytes  ($js)"
echo "Minified size:$(cat public/$min | wc -c) bytes  ($min)"
echo "Gzipped size: $(cat public/$gzip | wc -c) bytes ($gzip)" 

# remove tmp files
rm -f public/$js